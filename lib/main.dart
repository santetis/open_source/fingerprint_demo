import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';
import 'package:local_auth/error_codes.dart' as auth_error;

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  MyAppState createState() {
    return new MyAppState();
  }
}

class MyAppState extends State<MyApp> {
  bool loading = true;
  bool dontSupportBiometric = true;
  LocalAuthentication localAuth;

  @override
  void initState() {
    super.initState();
    checkBiometric();
  }

  Future<void> checkBiometric() async {
    localAuth = LocalAuthentication();

    List<BiometricType> availableBiometrics =
        await localAuth.getAvailableBiometrics();

    if (availableBiometrics.contains(BiometricType.fingerprint)) {
      dontSupportBiometric = false;
    } else {
      dontSupportBiometric = true;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(
        title: 'Flutter Demo Home Page',
        supportBiometric: !dontSupportBiometric,
        localAuthentication: localAuth,
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({
    Key key,
    this.title,
    this.supportBiometric,
    this.localAuthentication,
  }) : super(key: key);

  final bool supportBiometric;
  final String title;
  final LocalAuthentication localAuthentication;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool isAuthenticated = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: [
          widget.supportBiometric ? Text('Yes') : Text('No'),
          isAuthenticated ? Text('Vous etes authentifier') : Text('Anonymes'),
        ],
      ),
      floatingActionButton: widget.supportBiometric
          ? FloatingActionButton(
              child: Icon(Icons.fingerprint),
              onPressed: () async {
                try {
                  bool didAuthenticate = await widget.localAuthentication
                      .authenticateWithBiometrics(
                          localizedReason:
                              'Please authenticate to show account balance');
                  setState(() {
                    isAuthenticated = didAuthenticate;
                  });
                } on PlatformException catch (e) {
                  if (e.code == auth_error.notAvailable) {
                    // Handle this exception here.
                    print('error');
                  }
                }
              },
            )
          : null,
    );
  }
}
